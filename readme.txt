Espinosa Project Web Services Arena 3
Project started 29.12.2009. First success 30.12.2009 (JBossWS Native)
Reorganize wsarena2 project. Split to Maven sub-projects (modules).
Modules:
 - Service - server code, POJO, WS annotated. See pom.xml - there is no WS stuff! Server agnostic, pure SUN Java.
 - webservice - web service related stuff. Exmaple page. Define context. Define servlets. Server (JBoss) related stuff. See pom.xml - there is no WS stuff!
 - client - test client. There is no main stuff, only test subdir. See pom.xml - there is all the WS stuff, web service stub generating mojos, WSDL generation etc.
 
 NOTES:
 Namespace URI "http://service.wsarena.home.esp/" was uatogenerated! From seervice POJO classpath. It should be redefined by some anotation to less Javvy.
 
 
 Maven features
 ==============
 - my first modularized project. For very fisrt time - sub-project are in sub directories of the parent project. It works!
 - rename final WAR.
 - JBoss deploy made by profile "jboss_deploy". This was the only way how to make deploy only the webservice project.
 - I had a big Maven issue (30.12.2009) with WAR as dependancy! There are big limitations. webservice plugin mojos cannot see classes from WAR module, 
   and I need tme to see SEI implementation. So I had to split the Service (the POJO) subproject.
    