
package esp.home.wsarena.client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the esp.home.wsarena.client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _FooMethod1Response_QNAME = new QName("http://service.wsarena.home.esp/", "fooMethod1Response");
    private final static QName _FooMethod1_QNAME = new QName("http://service.wsarena.home.esp/", "fooMethod1");
    private final static QName _FooMethod2_QNAME = new QName("http://service.wsarena.home.esp/", "fooMethod2");
    private final static QName _FooMethod2Response_QNAME = new QName("http://service.wsarena.home.esp/", "fooMethod2Response");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: esp.home.wsarena.client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FooMethod2Response }
     * 
     */
    public FooMethod2Response createFooMethod2Response() {
        return new FooMethod2Response();
    }

    /**
     * Create an instance of {@link FooMethod1Response }
     * 
     */
    public FooMethod1Response createFooMethod1Response() {
        return new FooMethod1Response();
    }

    /**
     * Create an instance of {@link FooMethod2 }
     * 
     */
    public FooMethod2 createFooMethod2() {
        return new FooMethod2();
    }

    /**
     * Create an instance of {@link FooMethod1 }
     * 
     */
    public FooMethod1 createFooMethod1() {
        return new FooMethod1();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FooMethod1Response }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.wsarena.home.esp/", name = "fooMethod1Response")
    public JAXBElement<FooMethod1Response> createFooMethod1Response(FooMethod1Response value) {
        return new JAXBElement<FooMethod1Response>(_FooMethod1Response_QNAME, FooMethod1Response.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FooMethod1 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.wsarena.home.esp/", name = "fooMethod1")
    public JAXBElement<FooMethod1> createFooMethod1(FooMethod1 value) {
        return new JAXBElement<FooMethod1>(_FooMethod1_QNAME, FooMethod1 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FooMethod2 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.wsarena.home.esp/", name = "fooMethod2")
    public JAXBElement<FooMethod2> createFooMethod2(FooMethod2 value) {
        return new JAXBElement<FooMethod2>(_FooMethod2_QNAME, FooMethod2 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FooMethod2Response }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.wsarena.home.esp/", name = "fooMethod2Response")
    public JAXBElement<FooMethod2Response> createFooMethod2Response(FooMethod2Response value) {
        return new JAXBElement<FooMethod2Response>(_FooMethod2Response_QNAME, FooMethod2Response.class, null, value);
    }

}
