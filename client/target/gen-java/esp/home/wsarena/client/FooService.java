
package esp.home.wsarena.client;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.1.7-b01-
 * Generated source version: 2.1
 * 
 */
@WebService(name = "FooService", targetNamespace = "http://service.wsarena.home.esp/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface FooService {


    /**
     * 
     * @param arg0
     */
    @WebMethod
    @RequestWrapper(localName = "fooMethod1", targetNamespace = "http://service.wsarena.home.esp/", className = "esp.home.wsarena.client.FooMethod1")
    @ResponseWrapper(localName = "fooMethod1Response", targetNamespace = "http://service.wsarena.home.esp/", className = "esp.home.wsarena.client.FooMethod1Response")
    public void fooMethod1(
        @WebParam(name = "arg0", targetNamespace = "")
        String arg0);

    /**
     * 
     * @param arg0
     * @return
     *     returns java.lang.String
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "fooMethod2", targetNamespace = "http://service.wsarena.home.esp/", className = "esp.home.wsarena.client.FooMethod2")
    @ResponseWrapper(localName = "fooMethod2Response", targetNamespace = "http://service.wsarena.home.esp/", className = "esp.home.wsarena.client.FooMethod2Response")
    public String fooMethod2(
        @WebParam(name = "arg0", targetNamespace = "")
        String arg0);

}
