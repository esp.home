
package esp.home.wsarena.service.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "fooMethod1Response", namespace = "http://service.wsarena.home.esp/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fooMethod1Response", namespace = "http://service.wsarena.home.esp/")
public class FooMethod1Response {


}
