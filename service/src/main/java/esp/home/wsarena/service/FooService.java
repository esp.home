package esp.home.wsarena.service;

import java.util.logging.Logger;

import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 * Web service implementation
 * @author espinosa 27.11.2009
 */
@WebService(targetNamespace="http://")
public class FooService {
	public static final Logger log = Logger.getLogger(FooService.class.getName());
	
	@WebMethod
	public void fooMethod1(String command) {
		log.info("fooMethod2: " + command);
	}
	
	@WebMethod
	public String fooMethod2(String command) {
		log.info("fooMethod2: " + command);
		return "OK";
	}
}
